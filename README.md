# jason_schemer

A library for Dart developers. It is awesome.

## Usage

A simple usage example:

    import 'package:jason_schemer/jason_schemer.dart';

    main() {
      var awesome = new Awesome();
    }

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: http://example.com/issues/replaceme
