import 'package:stuff/stuff.dart';
import 'dart:async';
import 'model_impl.dart';
import 'package:built_collection/built_collection.dart';

enum SchemaType { array, boolean, integer, number, object, string }

typedef Future<Schema> SchemaResolutionContext(Uri schemaUri);

abstract class SchemaThingy implements Jsonable {
  Future<Schema> resolve(SchemaResolutionContext context);
}


abstract class Schema extends SchemaThingy {
  SchemaType get type;

  BuiltMap<String, Property> get properties;
  bool get additionalProperties;
  Iterable<String> get required;

//  "patternProperties": {
//  "^(/[^/]+)+$": { "$ref": "http://some.site.somewhere/entry-schema#" }
//  },

  Map toJson();

  factory Schema(SchemaType type, BuiltMap<String, Property> properties,
      Iterable<String> required, bool additionalProperties) = SchemaImpl;

  factory Schema.fromJson(Map json) = SchemaImpl.fromJson;
}

abstract class Property {
  Iterable<PropertyConstraint> get constraints;
  SchemaThingy get schemaHandle;

  factory Property(
          Iterable<PropertyConstraint> constraints, SchemaThingy schemaHandle) =
      PropertyImpl;
}

abstract class SchemaRef extends SchemaThingy {
  Uri get schemaUri;

  factory SchemaRef(Uri schemaUri) = SchemaRefImpl;
}

abstract class PropertyConstraint {}
