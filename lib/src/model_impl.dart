import 'package:stuff/stuff.dart';
import 'package:jason_schemer/src/model.dart';
import 'package:built_collection/built_collection.dart';
import 'dart:async';

class SchemaImpl implements Schema, Jsonable {
  @override
  final SchemaType type;

  @override
  final BuiltMap<String, Property> properties;

  Map<String, Property> get _propertiesMap => properties.toMap();

  @override
  final Iterable<String> required;

  @override
  final bool additionalProperties;

  final Map _unParsed;

  SchemaImpl(this.type, this.properties, Iterable<String> required,
      this.additionalProperties, [this._unParsed = const {}])
      : this.required = required is BuiltList<String>
            ? required
            : new BuiltList<String>(required);

  SchemaImpl._fromJson(JsonParser p)
      : this(
            p.single(
                'type',
                (v) => SchemaType.values
                    .firstWhere((st) => st.toString().endsWith(v))),
            new BuiltMap<String, Property>(
                p.mapValues('properties', (v) => new PropertyImpl.fromJson(v))),
            p.list('required') as Iterable<String>,
            p.single('additionalProperties'),
            p.unconsumed);

  SchemaImpl.fromJson(Map json)
      : this._fromJson(parseJson(json, consumeMap: true));

  Map toJson() => (buildJson
        ..add('type', type, (t) => t.toString().substring('SchemaType.'.length))
        ..add('properties', _propertiesMap)
        ..add('required', required)
        ..add('additionalProperties', additionalProperties)
        ..addAll(_unParsed))
      .json;

  @override
  Future<Schema> resolve(SchemaResolutionContext context) async => this;
}

class PropertyImpl implements Property, Jsonable {
  @override
  final Iterable<PropertyConstraint> constraints;

  @override
  final SchemaThingy schemaHandle;

  final Map _unParsed;

  PropertyImpl(Iterable<PropertyConstraint> constraints, this.schemaHandle,
      [this._unParsed = const {}])
      : this.constraints = constraints is BuiltList<PropertyConstraint>
            ? constraints
            : new BuiltList<PropertyConstraint>(constraints);

  PropertyImpl._fromJson(JsonParser p)
      : this(const [], _parseSchemaHandle(p), p.unconsumed);

  PropertyImpl.fromJson(Map json)
      : this._fromJson(parseJson(json, consumeMap: true));

  @override
  Map toJson() => (buildJson
//        ..addAll(convertConstraints)
        ..addAll(schemaHandle.toJson())
        ..addAll(_unParsed))
      .json;
}

SchemaThingy _parseSchemaHandle(JsonParser p) {
  return p.unconsumed.containsKey(r'$ref')
      ? new SchemaRefImpl._fromJson(p)
      : new SchemaImpl._fromJson(p);
}

class SchemaRefImpl implements SchemaRef, Jsonable {
  @override
  final Uri schemaUri;

  SchemaRefImpl(this.schemaUri);

  SchemaRefImpl._fromJson(JsonParser p)
      : this(p.single(r'$ref', (v) => Uri.parse(v)));

  @override
  Map toJson() => (buildJson..add(r'$ref', schemaUri)).json;

  @override
  Future<Schema> resolve(SchemaResolutionContext context) async =>
      throw new StateError('not yet implemented');
}
