// Copyright (c) 2016, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library jason_schemer.test;

import 'package:jason_schemer/src/model.dart';
import 'package:test/test.dart';

void main() {
  final contextSchema = {
    "type": "object",
    "properties": {
      "tenantId": {"type": "integer"},
      "projectKey": {"type": "string"},
      "issueKey": {"type": "string"}
    },
    "required": ["tenantId"]
  };

  final extensionSchema = {
    "type": "object",
    "properties": {
      "moduleKey": {"type": "string"},
      "description": {"type": "string"}
//        ,
//        "url": {r"$ref": "#/definitions/signedUriTemplate"}
    },
    "required": ["moduleKey"]
  };

  group('A group of tests', () {
    test('', () {
      expect(new Schema.fromJson(contextSchema).toJson(), contextSchema);
    });
  });
}
